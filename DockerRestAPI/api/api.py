# Laptop Service

from flask import Flask, request
from flask_restful import Resource, Api
from pymongo import MongoClient

# Instantiate the app
app = Flask(__name__)
api = Api(app)
client = MongoClient("db", 27017)
db = client.tododb

# Use as reference
# class Laptop(Resource):
#     def get(self):
#         return {
#             'Laptops': ['Mac OS', 'Dell', 
#             'Windozzee',
# 	    'Yet another laptop!',
# 	    'Yet yet another laptop!'
#             ]
#         }

class _listAll(Resource):
	def get(self):
		result_list = []
		results = request.args.get("top")
		if results == None:
			results = 500

		getDB = db.tododb.find().sort("_open_time", 1).limit(int(results))
		for item in getDB:
			result_list.append(item)

		return {
			'_open_time': [i['_open'] for i in result_list],
			'_close_time': [i['_close'] for i in result_list]
		}

class _listOpenOnly(Resource):
	def get(self):
		result_list = []
		results = request.args.get("top")
		if results == None:
			results = 500

		getDB = db.tododb.find().sort("_open_time", 1).limit(int(results))
		for item in getDB:
			result_list.append(item)

		return {
			'_open_time': [i['_open'] for i in result_list]
		}

class _listCloseOnly(Resource):
	def get(self):
		result_list = []
		results = request.args.get("top")
		if results == None:
			results = 500

		getDB = db.tododb.find().sort("_open_time", 1).limit(int(results))
		for item in getDB:
			result_list.append(item)

		return {
			'_close_time': [i['_close'] for i in result_list]
		}

class _listAllJson(Resource):
	def get(self):
		result_list = []
		results = request.args.get("top")
		if results == None:
			results = 500

		getDB = db.tododb.find().sort("_open_time", 1).limit(int(results))
		for item in getDB:
			result_list.append(item)

		return {
			'_open_time': [i['_open'] for i in result_list],
			'_close_time': [i['_close'] for i in result_list]
		}

class _listOpenOnlyJson(Resource):
	def get(self):
		result_list = []
		results = request.args.get("top")
		if results == None:
			results = 500

		getDB = db.tododb.find().sort("_open_time", 1).limit(int(results))
		for item in getDB:
			result_list.append(item)

		return {
			'_open_time': [i['_open'] for i in result_list]
		}

class _listCloseOnlyJson(Resource):
	def get(self):
		result_list = []
		results = request.args.get("top")
		if results == None:
			results = 500

		getDB = db.tododb.find().sort("_open_time", 1).limit(int(results))
		for item in getDB:
			result_list.append(item)

		return {
			'_close_time': [i['_close'] for i in result_list]
		}

# From my understanding csv should be a couple of lines changed, nothing more.

class _listAllCsv(Resource):
	def get(self):
		retStr = ""

		results = request.args.get("top")
		if results == None:
			results = 500

		getDB = db.tododb.find().sort("_open_time", 1).limit(int(results))
		for item in getDB:
			retStr += item['_open'] + ', ' + item['_close'] + ', '

		return retStr

class _listOpenOnlyCsv(Resource):
	def get(self):
		retStr = ""

		results = request.args.get("top")
		if results == None:
			results = 500

		getDB = db.tododb.find().sort("_open_time", 1).limit(int(results))
		for item in getDB:
			retStr += item['_open'] + ', ' 

		return retStr


class _listCloseOnlyCsv(Resource):
	def get(self):
		retStr = ""

		results = request.args.get("top")
		if results == None:
			results = 500

		getDB = db.tododb.find().sort("_open_time", 1).limit(int(results))
		for item in getDB:
			retStr += item['_close'] + ', '

		return retStr

# Create routes
# Another way, without decorators
#api.add_resource(Laptop, '/')

api.add_resource(_listAll, '/listAll')
api.add_resource(_listOpenOnly, '/listOpenOnly')
api.add_resource(_listCloseOnly, '/listCloseOnly')
api.add_resource(_listAllJson, '/listAll/json')
api.add_resource(_listOpenOnlyJson, '/listOpenOnly/json')
api.add_resource(_listCloseOnlyJson, '/listCloseOnly/json')
api.add_resource(_listAllCsv, '/listAll/csv')
api.add_resource(_listOpenOnlyCsv, '/listOpenOnly/csv')
api.add_resource(_listCloseOnlyCsv, '/listCloseOnly/csv')

# Run the application
if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True)
