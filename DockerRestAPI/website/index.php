<html>
    <head>
        <title>Howdy welcome to project 6</title>
    </head>

    <body>
        <h1>Times display (ACP Times)</h1>
        <ul>
            <h1>listAll:</h1>
            <?php
            $json = file_get_contents('http://api:5000/listAll');
            $obj = json_decode($json);
	          $_open_time = $obj->_open_time;
              $_close_time = $obj->_close_time;
            echo "  The times!";
            echo "  Opening time(s):";
            foreach ($_open_time as $l) {
                echo "<li>$l</li>";
            }
            echo "  Closing time(s):";
            foreach ($_close_time as $l) {
                echo "<li>$l</li>";
            }
            ?>

            <h1>listOpenOnly:</h1>
            <?php
            $json = file_get_contents('http://api:5000/listOpenOnly');
            $obj = json_decode($json);
              $_open_time = $obj->_open_time;
            echo "  The times!";
            echo "  Opening time(s):";
            foreach ($_open_time as $l) {
                echo "<li>$l</li>";
            }
            ?>

            <h1>listCloseOnly:</h1>
            <?php
            $json = file_get_contents('http://api:5000/listCloseOnly');
            $obj = json_decode($json);
              $_close_time = $obj->_close_time;
            echo "  The times!";
            echo "  closing time(s):";
            foreach ($_close_time as $l) {
                echo "<li>$l</li>";
            }
            ?>

            <h1>listAll/json:</h1>
            <?php
            $json = file_get_contents('http://api:5000/listAll/json');
            $obj = json_decode($json);
              $_open_time = $obj->_open_time;
              $_close_time = $obj->_close_time;
            echo "  The times!";
            echo "  Opening time(s):";
            foreach ($_open_time as $l) {
                echo "<li>$l</li>";
            }
            echo "  Closing time(s):";
            foreach ($_close_time as $l) {
                echo "<li>$l</li>";
            }
            ?>

            <h1>listOpenOnly/json:</h1>
            <?php
            $json = file_get_contents('http://api:5000/listOpenOnly/json');
            $obj = json_decode($json);
              $_open_time = $obj->_open_time;
            echo "  The times!";
            echo "  Opening time(s):";
            foreach ($_open_time as $l) {
                echo "<li>$l</li>";
            }
            ?>

            <h1>listCloseOnly/json:</h1>
            <?php
            $json = file_get_contents('http://api:5000/listCloseOnly/json');
            $obj = json_decode($json);
              $_close_time = $obj->_close_time;
            echo "  The times!";
            echo "  closing time(s):";
            foreach ($_close_time as $l) {
                echo "<li>$l</li>";
            }
            ?>

            <h1>listAll/csv:</h1>
            <?php
            echo file_get_contents('http://api:5000/listAll/csv');
            ?>

            <h1>listOpenOnly/csv:</h1>
            <?php
            echo file_get_contents('http://api:5000/listOpenOnly/csv');
            ?>

            <h1>listCloseOnly/csv:</h1>
            <?php
            echo file_get_contents('http://api:5000/listCloseOnly/csv');
            ?>

            <h1>You can also use ?top=[some # here] at the end of all of the above queries</h1>
            <h1>For example:</h1>
            <h1>listOpenOnly/csv?top=3:</h1>
            <?php
            echo file_get_contents('http://api:5000/listOpenOnly/csv?top=3');
            ?>



        </ul>
    </body>
</html>
