import os
import arrow
import flask
import acp_times
from flask import Flask, redirect, url_for, request, render_template
from pymongo import MongoClient

app = Flask(__name__)

client = MongoClient('db', 27017)
db = client.tododb
db.tododb.delete_many({})

@app.route("/")
@app.route("/index")
def index():
    app.logger.debug("Main page entry")
    return render_template('calc.html')

@app.errorhandler(404)
def page_is_not_found(error):
    app.logger.debug("Page not found")
    return flask.render_template('404.html'), 404

@app.route("/_calc_times")
def _calc_times():
    """
    Calculates open/close times from miles, using rules
    described at https://rusa.org/octime_alg.html.
    Expects one URL-encoded argument, the number of miles.
    """
    app.logger.debug("Got a JSON request")
    km = request.args.get('km', 999, type=float)
    app.logger.debug("km={}".format(km))
    app.logger.debug("request.args: {}".format(request.args))
    # FIXME: These probably aren't the right open and close times
    # and brevets may be longer than 200km
    startDate = request.args.get('begin_date', "2017-01-01", type=str) + ' ' + request.args.get('begin_time', "00:00", type=str) + '-08:00'
    startArrow = arrow.get(startDate, "YYYY-MM-DD hh:mmZZ")
    brevDist = request.args.get('brevet_dist_km', 200, type=float)

    open_time = acp_times.open_time(km, brevDist, startArrow.isoformat())
    close_time = acp_times.close_time(km, brevDist, startArrow.isoformat())
    result = {"open": open_time, "close": close_time}
    return flask.jsonify(result=result)

@app.route('/display', methods=['POST'])
def display():
    items = [item for item in db.tododb.find()]
    comparison = len(items)
    if (comparison != 0):
        return render_template('display.html', items=items)
    else:
        return render_template('error.html')

@app.route('/submit', methods=['POST'])
def submit():
    _dist = request.form.getlist("km")
    _close = request.form.getlist("close")
    _open = request.form.getlist("open")

    strO = []
    strC = []
    strD = []

    for i in _dist:
        temp = str(i)
        if (temp != ""):
            strD.append(temp)

    for i in _close:
        temp = str(i)
        if (temp != ""):
            strC.append(temp)

    for i in _open:
        temp = str(i)
        if (temp != ""):
            strO.append(temp)

    largeLen = max(len(strD), len(strC), len(strO))

    if largeLen == 0:
        return render_template('error.html')

    for j in range(largeLen):
        item_doc = {
            '_open': strO[j],
            '_close': strC[j],
            '_dist': strD[j]
        }
        db.tododb.insert_one(item_doc)
    return redirect(url_for('index'))

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)
